'use strict';
~ function() {
  var
  easeInOut = Power1.easeInOut,
  bgExit= document.getElementById('bgExit'),
  tl = new TimelineMax();

    window.init = function(){
      tl.to('#rect',1.5,{strokeDashoffset:440})
      tl.add(verticalLines,'2')
      tl.add(horizontalLines,'2')
      tl.to('#box',1,{rotation:-45,ease:easeInOut},'4')
    }

   

function verticalLines(){
  var line;
  var tl = new TimelineMax();
  for(var i=0; i<= 3; i++){
    line = document.createElement('div');
    line.id='line'+i;
    line.className='drawLine';
    line.style.left = 28*i +'px';
    document.getElementById('vLines').appendChild(line);
    tl.to(line,0.5,{height:136},'-=0.2');
  }
}

function horizontalLines(){
  var linee;
  var t2 = new TimelineMax();
  for(var i=0; i<=3; i++){
    linee = document.createElement('div');
    linee.id='hline'+i;
    linee.className='drawhLine';
    linee.style.top = 28*i +'px';
    document.getElementById('hLines').appendChild(linee);
  t2.to(linee,0.5,{width:136},'-=0.2');
  }
}
}();